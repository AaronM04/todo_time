Changelog
=========

### v0.2.0, 2022-10-30

* Specially treat lines starting with `n --gettime`. These lines are _not_
  removed, unlike the general case for lines starting with `n --`; instead,
  they are modified to indicate the calculated time so far.

### v0.1.0, 2022-04-23

* Tally up time for all input lines starting with certain characters (` `, `i`,
  etc.) and containing ` = ` before the time.

* Align lines.

* Also align an optional importance after the time, represented as any number
  of asterisks

* Time total at bottom shows start time and end time as well as total time.
