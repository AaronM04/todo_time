use chrono::{DateTime, Duration, Local, TimeZone};
use lazy_static::lazy_static;
use regex::Regex;

use std::env::args;
use std::io::{self, Write};
use std::ops::{Add, AddAssign};

const PREFIX: &str = "n --";
const CHRONO_FORMAT_STR: &str = "%l:%M%P";
const CHRONO_DATETIME_FORMAT_STR: &str = "%F %l:%M%P"; // same as above, but with "YYYY-MM-DD " prepended

fn main() {
    // Show version with -v
    if let Some(firstarg) = args().nth(1) {
        if firstarg.trim() == "-v" {
            let ver = env!("CARGO_PKG_VERSION");
            let mut stderr_handle = io::stderr().lock();
            let _ = stderr_handle.write_all(format!("v{}\n", ver).as_bytes());
            return;
        }
    }

    let stdin = io::stdin();
    let stdout = io::stdout();
    let mut stdout_handle = stdout.lock();

    let mut line = String::new();
    let mut now = Local::now();
    let mut total_duration = Dur::default();
    let mut lines = vec![];
    while let Ok(len) = stdin.read_line(&mut line) {
        // If EOF, done processing lines
        if len == 0 {
            break;
        }

        // Echo current line assuming it's not a duration total.
        // Modify line if it's a gettime command
        if line.starts_with(PREFIX) {
            let parts: Vec<_> = line
                .split_once(PREFIX)
                .expect("contains PREFIX")
                .1
                .trim()
                .split_whitespace()
                .collect();
            if parts.len() >= 1 {
                match parts[0] {
                    "gettime" => lines.push(gettime(now, total_duration)),
                    "settime" => {
                        settime(&mut now, parts.iter().nth(1).map(|p| *p));
                        total_duration = Dur::default(); // Reset added-up duration
                        lines.push(line.clone());
                    }
                    _ => {
                        // Ignore for now; ToDo: other variants
                    }
                }
            }
        } else {
            lines.push(line.clone());
        }

        // Add duration to total, if there is a duration for this line
        if let Some(dur) = duration_of_input_line(&line) {
            total_duration += dur;
        }

        // Clear, since stdin.read_line(...) doesn't do this for us
        line.clear();
    }
    let total_str = get_total_str(now, total_duration);
    lines.push(total_str);

    // Reformat the = and the ***
    align(&mut lines);

    // Now, write all to stdout
    for line in lines {
        stdout_handle
            .write_all(line.as_bytes())
            .expect("write bytes to stdout");
    }
}

fn gettime(now: DateTime<Local>, total_duration: Dur) -> String {
    let time_so_far = completion_time_as_str(now, total_duration);
    format!("{} gettime -> {}\n", PREFIX, time_so_far)
}

fn settime(now: &mut DateTime<Local>, time_str: Option<&str>) {
    if time_str.is_none() {
        return; // ToDo: handle error
    }

    // unwrap OK below because of check above
    let datetime_str = format!("2020-01-01 {}", time_str.unwrap());

    match Local.datetime_from_str(&datetime_str, CHRONO_DATETIME_FORMAT_STR) {
        Ok(new_now) => {
            *now = new_now;
        }
        Err(_e) => {
            return; // ToDo: handle error
        }
    }
}

fn completion_time_as_str(now: DateTime<Local>, total_duration: Dur) -> String {
    let cdur = Duration::hours(total_duration.hours as i64)
        + Duration::minutes(total_duration.minutes as i64);
    (now + cdur).format(CHRONO_FORMAT_STR).to_string()
}

fn get_total_str(now: DateTime<Local>, total_duration: Dur) -> String {
    let completion_time = completion_time_as_str(now, total_duration);
    let total_str = format!(
        "{}{} + {} -> {}\n",
        PREFIX,
        now.format(CHRONO_FORMAT_STR),
        total_duration.encode(),
        completion_time
    );
    total_str
}

/// If the line is a task with duration, like "pay taxes = 7hr", return that duration
fn duration_of_input_line(line: &str) -> Option<Dur> {
    if let Some(ch) = line.chars().next() {
        if ch == 'x' || ch == '!' {
            // This task is done or skipped
            return None;
        }
    }
    if let Some((_, after_equals)) = line.split_once(" = ") {
        Some(Dur::decode(after_equals))
    } else {
        None
    }
}

#[derive(Debug, Clone, Copy, Eq, PartialEq, Default)]
struct Dur {
    hours: isize,
    minutes: isize,
}

impl AddAssign for Dur {
    fn add_assign(&mut self, other: Dur) {
        self.hours += other.hours;
        self.minutes += other.minutes;
        while self.minutes >= 60 {
            // Sub-optimal but too lazy for now :/
            self.minutes -= 60;
            self.hours += 1;
        }
    }
}

impl Add for Dur {
    type Output = Dur;
    fn add(self, other: Dur) -> Dur {
        let mut result = self;
        result += other;
        result
    }
}

impl Dur {
    /// Converts something like "2h15m" to Dur{hours:2, minutes:15}
    fn decode(s: &str) -> Dur {
        lazy_static! {
            static ref H_RE: Regex = Regex::new(r"(\d+) ?hr?").expect("H_RE compile");
            static ref M_RE: Regex = Regex::new(r"(\d+) ?m(in)?").expect("M_RE compile");
        }
        let mut hours: isize = 0;
        let mut minutes: isize = 0;
        let h_caps = H_RE.captures(s);
        if let Some(h_caps) = &h_caps {
            hours = h_caps[1].parse().expect("parse hour number");
        }
        let m_caps = M_RE.captures(s);
        if let Some(m_caps) = &m_caps {
            minutes = m_caps[1].parse().expect("parse minute number");
        }
        if h_caps.is_none() && m_caps.is_none() {
            panic!("neither hours nor minutes in {:?}", s);
        }
        Dur { hours, minutes }
    }

    /// Converts something like Dur{hours:2, minutes:15} to "2h15m"
    fn encode(self) -> String {
        if self.hours != 0 && self.minutes == 0 {
            format!("{}hr", self.hours)
        } else if self.hours != 0 {
            format!("{}h{}min", self.hours, self.minutes)
        } else {
            format!("{}min", self.minutes)
        }
    }
}

lazy_static! {
    /// 3 capture groups: before equals, between equals and stars, and stars (if any)
    static ref ALIGN_RE: Regex = Regex::new(r"^(.*) += ([^*\n]+)(\*+)?\n?$").expect("RE compile");
}

/// Adds or removes spaces in two locations: preceding an equals sign and following an equals sign.
fn align(lines: &mut Vec<String>) {
    // First, get the maximum lengths of the first two groups
    let mut max_len_before_equals: Option<usize> = None;
    let mut max_len_after_equals: Option<usize> = None;
    for line in lines.iter() {
        let caps = ALIGN_RE.captures(line);
        if caps.is_none() {
            continue;
        }
        let caps = caps.unwrap();

        let cap1 = caps.get(1).expect("capture 1").as_str().trim_end(); // Right align
        let cap2 = caps.get(2).expect("capture 2").as_str().trim(); // Left align

        if let Some(ml) = max_len_before_equals.as_mut() {
            if *ml < cap1.len() {
                *ml = cap1.len();
            }
        } else {
            max_len_before_equals = Some(cap1.len());
        }

        if let Some(ml) = max_len_after_equals.as_mut() {
            if *ml < cap2.len() {
                *ml = cap2.len();
            }
        } else {
            max_len_after_equals = Some(cap2.len());
        }
    }

    if max_len_before_equals.is_none() || max_len_after_equals.is_none() {
        // Nothing to do
        return;
    }
    let max_len_before_equals = max_len_before_equals.unwrap();
    let max_len_after_equals = max_len_after_equals.unwrap();

    // Then, modify the lines to add or remove spacing
    for line in lines {
        let original_line = line.clone();
        let caps = ALIGN_RE.captures(&original_line);
        if caps.is_none() {
            continue;
        }
        let caps = caps.unwrap();

        let cap1 = caps.get(1).expect("capture 1").as_str().trim_end(); // Right align
        let cap2 = caps.get(2).expect("capture 2").as_str().trim(); // Left align
        let opt_cap3 = caps.get(3);

        line.clear();

        // Left align
        line.push_str(cap1);
        for _ in 0..max_len_before_equals - cap1.len() {
            line.push(' ');
        }

        line.push_str(" = ");

        // Right align
        for _ in 0..max_len_after_equals - cap2.len() {
            line.push(' ');
        }
        line.push_str(cap2);

        if let Some(cap3) = opt_cap3 {
            line.push(' ');
            line.push_str(cap3.as_str());
        }
        line.push('\n');
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn align_re_matches() {
        let s = "long_line =  5min *\n";
        assert!(ALIGN_RE.captures(s).is_some());
    }

    #[test]
    fn align_inserts_before_equals() {
        let mut lines: Vec<String> =
            vec!["long_line =  5min *\n".into(), "foo = 10min **\n".into()];
        let expected: Vec<String> = vec![
            "long_line =  5min *\n".into(),
            "foo       = 10min **\n".into(),
        ];
        align(&mut lines);
        assert_eq!(lines, expected);
    }

    #[test]
    fn align_removes_before_equals() {
        let mut lines: Vec<String> = vec![
            "long_line =  5min *\n".into(),
            "foo          = 10min **\n".into(),
        ];
        let expected: Vec<String> = vec![
            "long_line =  5min *\n".into(),
            "foo       = 10min **\n".into(),
        ];
        align(&mut lines);
        assert_eq!(lines, expected);
    }

    #[test]
    fn align_inserts_after_equals() {
        let mut lines: Vec<String> = vec![
            "long_line = 5min *\n".into(),
            "foo       = 10min **\n".into(),
        ];
        let expected: Vec<String> = vec![
            "long_line =  5min *\n".into(),
            "foo       = 10min **\n".into(),
        ];
        align(&mut lines);
        assert_eq!(lines, expected);
    }

    #[test]
    fn align_removes_after_equals() {
        let mut lines: Vec<String> = vec![
            "long_line =      5min *\n".into(),
            "foo       = 10min **\n".into(),
        ];
        let expected: Vec<String> = vec![
            "long_line =  5min *\n".into(),
            "foo       = 10min **\n".into(),
        ];
        align(&mut lines);
        assert_eq!(lines, expected);
    }

    #[test]
    fn align_handles_no_stars() {
        let mut lines: Vec<String> = vec![
            "long_line =  5min *\n".into(),
            "foo          = 10min **\n".into(),
            "baz    =  5min\n".into(),
        ];
        let expected: Vec<String> = vec![
            "long_line =  5min *\n".into(),
            "foo       = 10min **\n".into(),
            "baz       =  5min\n".into(),
        ];
        align(&mut lines);
        assert_eq!(lines, expected);
    }

    #[test]
    fn align_already_aligned_no_change() {
        let mut lines: Vec<String> = vec![
            "long_line =  5min *\n".into(),
            "foo       = 10min **\n".into(),
        ];
        let expected = lines.clone();
        align(&mut lines);
        assert_eq!(lines, expected);
    }

    #[test]
    fn encode_dur() {
        let d = Dur {
            hours: 2,
            minutes: 15,
        };
        let expected = "2h15min".to_string();
        assert_eq!(d.encode(), expected);
    }

    #[test]
    fn decode_dur_zero_hr() {
        let mut s = "15m";
        let expected = Dur {
            hours: 0,
            minutes: 15,
        };
        assert_eq!(Dur::decode(&s), expected);

        // another variation
        s = "15min";
        assert_eq!(Dur::decode(&s), expected);
    }

    #[test]
    fn decode_dur_zero_min() {
        let mut s = "2h";
        let expected = Dur {
            hours: 2,
            minutes: 0,
        };
        assert_eq!(Dur::decode(&s), expected);

        // another variation
        s = "2hr";
        assert_eq!(Dur::decode(&s), expected);
    }

    #[test]
    fn decode_dur() {
        let mut s = "2h15m";
        let expected = Dur {
            hours: 2,
            minutes: 15,
        };
        assert_eq!(Dur::decode(&s), expected);

        // another variation
        s = "2hr15m";
        assert_eq!(Dur::decode(&s), expected);

        // yet another variation
        s = "2hr15min";
        assert_eq!(Dur::decode(&s), expected);
    }
}
